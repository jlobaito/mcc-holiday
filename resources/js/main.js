// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
                  =================================== */
                  var ul = document.querySelector('.sweaters ul');
                  for (var i = ul.children.length; i >= 0; i--) {
                    ul.appendChild(ul.children[Math.random() * i | 0]);
                  }

                  $("ul li:nth-child(12n + 12)").each(function(i){
                    $(this).addClass("bg"+(i+1));
                  })



/*var bg = {
"g1":"$('.bg2').offset().top",
"g2":"$('.bg3').offset().top",
"g3":"$('.bg3').offset().top"
}*/

var g1 = $('.bg1').offset().top;
var g2 = $('.bg2').offset().top;
var g3 = $('.bg3').offset().top;
var g4 = $('.bg4').offset().top;
var g5 = $('.bg5').offset().top;
var g6 = $('.bg6').offset().top;

var body = $('body');

$(document).ready(function() {
    var bodyheight = $(window).height();
    $(".hidden-phone .left").height(bodyheight - 32);
});

// for the window resize
$(window).resize(function() {
    var bodyheight = $(window).height();
    $(".hidden-phone .left").height(bodyheight - 32);
});


$(window).scroll(function(){
  if($(window).width() >= 768){
    var current = $(this).scrollTop();
    if ( current > g1 && current < g2){ 
     body.removeClass();
     body.addClass('g1').css('background-color', '#F2F2F2');
     $('li.a2').addClass('active').siblings('li').removeClass('active active-a1');
   } else if ( current > g2 && current < g3){
     body.removeClass();
     body.addClass('g2').css('background-color', '#1A2B4A');
     $('li.a3').addClass('active').siblings('li').removeClass('active active-a1');
   } else if ( current > g3 && current < g4){
     body.removeClass();
     body.addClass('g3').css('background-color', '#447622');
     $('li.a4').addClass('active').siblings('li').removeClass('active active-a1');
   } else if ( current > g4 && current < g5){
     body.removeClass();
     body.addClass('g4').css('background-color', '#960066');
     $('li.a5').addClass('active').siblings('li').removeClass('active active-a1');
   } else if ( current > g5 ){
     body.removeClass();
     body.addClass('g5').css('background-color', '#C48726');
     $('li.a6').addClass('active').siblings('li').removeClass('active active-a1');
   } else if (current < g1){
    body.removeClass('g1', 'g2', 'g3', 'g4', 'g5').addClass('start').css('background-color', '#c73227');
    $('li.a1').addClass('active-a1').siblings('li').removeClass('active');
  }
  }
});



$(document).ready(function() {
  $('.sweaters li').bind('touchend', function(e) {

    $('.sweaters li .overlay').toggleClass('hover_effect');
  });
});
